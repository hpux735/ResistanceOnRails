namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
    puts "Making users"
    make_users
    puts "Making games"
    make_games
    puts "Making players table"
    make_players
  end
end

def make_users
  User.create!(name:  "William Dillon",
               email: "william@housedillon.com",
               password: "testpass",
               password_confirmation: "testpass",
               admin: true)
  User.create!(name:  "Katie Baker",
               email: "toastbaker@yahoo.com",
               password: "testpass",
               password_confirmation: "testpass")
  User.create!(name:  "Chris Chambers",
               email: "cwc1983@gmail.com",
               password: "testpass",
               password_confirmation: "testpass")
  User.create!(name:  "Eric Walkingshaw",
               email: "situationallefty@gmail.com",
               password: "testpass",
               password_confirmation: "testpass")
  User.create!(name:  "Allison Walkingshaw",
               email: "magicjohnston@hotmail.com",
               password: "testpass",
               password_confirmation: "testpass")
end

def make_games
  users = User.all(limit: 6)
  50.times do
    users.each { |user| user.games.create! }
  end
end

def make_players
  users = User.all
  games = Game.all
  games.each { |game|
    participants = users.shuffle[0..5]
    game.add_players(participants)
  }
end