class RemoveRelationshipsAndMicroposts < ActiveRecord::Migration
  def up
    drop_table :relationships
    drop_table :microposts
  end
  
  def down
    ActiveRecord::IrreversibleMigration
  end
end
