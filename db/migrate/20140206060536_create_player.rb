class CreatePlayer < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.integer :game
      t.integer :player
      t.integer :role
    end
    
    add_index(:players, :game)
    add_index(:players, :player)
  end
end
