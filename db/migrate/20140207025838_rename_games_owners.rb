class RenameGamesOwners < ActiveRecord::Migration
  def change
    remove_column :games, :owner
    add_column :games, :owner_id, :integer
  end
end
