class Game < ActiveRecord::Base
  belongs_to :owner, class_name: "User"
  default_scope -> { order('created_at DESC') }
  validates :owner_id, presence: true

#  has_many :playing_users, foreign_key: "user_id", class_name: "Player"
#  has_many :roles, through: :playing_users, source: :user


  has_many :players
  has_many :users, through: :players

  def summary
    "X players in round Y (game.rb)"
  end

  def add_players(users)
    
    users.each { |user|
      self.participant.create(user_id = user.id, role = "unset")
    }
  end

  def self.from_users_followed_by(user)
    followed_user_ids = "SELECT followed_id FROM relationships WHERE follower_id = :user_id"
    where("user_id IN (#{followed_user_ids}) OR user_id = :user_id", user_id: user)
  end

end
