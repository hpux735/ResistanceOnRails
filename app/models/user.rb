VALID_EMAIL_REGEX = /([\w+\-.]+)@([a-z0-9\-]+\.)+([a-z]+)/i

class User < ActiveRecord::Base

  # Database model relationships
  has_many :games, foreign_key: "owner_id" 

  has_many :players
  has_many :roles, through: :players
  
#  has_many :relationships, foreign_key: "follower_id", dependent: :destroy
#  has_many :reverse_relationships, foreign_key: "followed_id",
#                                   class_name:  "Relationship",
#                                   dependent:   :destroy
#  has_many :followed_users, through: :relationships, source: :followed
#  has_many :followers, through: :reverse_relationships, source: :follower

  
  
  # Before action callbacks
	before_save { email.downcase! }
	before_create :create_remember_token
	
	# Validation checks
	has_secure_password
	validates :password, length: { minimum: 6 }
	validates :name,  presence: true, length: { maximum: 50 }
	validates :email, presence: true, format:     { with: VALID_EMAIL_REGEX },
                        					  uniqueness: { case_sensitive: false }
		
	def User.new_remember_token
	  SecureRandom.urlsafe_base64
  end
  
  def User.encrypt(token)
    Digest::SHA1.hexdigest(token.to_s)
  end
	
	def played_games
	  #Game.played_by(self)
	  []
  end

	def owned_games
	  #Game.played_by(self)
	  []
  end
	
	def player?(game)
	  relationships.find_by(followed_id: other_user.id)
  end
  
  def spy?(game)
    relationships.create!(followed_id: other_user.id)
  end
	
	def voting?(game)
	  relationships.find_by(followed_id: other_user.id).destroy
	end
	
	private
	  def create_remember_token
	    self.remember_token = User.encrypt(User.new_remember_token)
	  end
end
