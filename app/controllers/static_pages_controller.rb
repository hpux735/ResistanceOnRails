class StaticPagesController < ApplicationController
  def home
    if signed_in?
      #@vote  = current_user.votes.build if signed_in?
      @feed_items = Game.paginate(page: params[:page])
    end
  end

  def help
  end
  
  def about
  end
  
  def contact
  end
end
