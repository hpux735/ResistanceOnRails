class SessionsController < ApplicationController

  def new
  end
  
  def create
    # Lookup the user by the sanitized email
    user = User.find_by_email(signin_params[:session][:email].downcase)
    
    # User not tound
    if user.nil?
      flash.now[:error] = "Username or email address incorrect"
      render 'new'
      return
    end
    
    # Password error
    if user.authenticate(params[:session][:password]).nil?
      flash.now[:error] = "Username or email address incorrect"
      render 'new'
      return
    end
    
    # We get here if it was a success 
    sign_in user
    redirect_back_or user
  end
  
  def destroy
    sign_out
    redirect_to root_path
  end

  private
    def signin_params
      params
    	#params[:session].require(:email).permit(:email)
    end

end
