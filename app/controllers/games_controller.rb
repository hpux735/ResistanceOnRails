class GamesController < ApplicationController
  before_action :signed_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: [:edit, :destroy, :update]

  def index
  end

  def played_by(id)
    return id == @owner_id
  end

  def summary
    "Fill this into the games_controller.rb file"
  end

  def show
    @game = Game.find(params[:id])
  end

  def create
    @game = current_user.games.build()
    if @game.save
      flash[:success] = "New game created!"
      redirect_to root_url
    else
      flash[:error] = "Unable to create game"
      redirect_to root_url
    end
  end

  def destroy
    @game.destroy
    redirect_to root_url
  end
  
  private
    def game_params
      params.require(:game).permit(:owner_id)
    end
    
    def correct_user
      @game = current_user.owned.find_by(id: params[:id])
      redirect_to root_url if @micropost.nil?
    end
end